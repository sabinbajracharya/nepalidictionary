//
//  File.swift
//  NepaliDictionary
//
//  Created by Sabin Bajracharya on 5/9/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

public class FileResource {
    
    public static func readDataFromFile(file:String) -> String!{
        guard let filePath = Bundle.init(for: self).url(forResource: file, withExtension: "csv")
        //guard let filePath = Bundle.main.path(forResource: "nepengmap", ofType: "csv")
            else {
                return nil
        }
        do {
            let contents = try Data(contentsOf: filePath)
            return String(data: contents, encoding: .utf8)
        } catch {
            print("File Read Error for file \(filePath)")
            return nil
        }
    }
    
    public static func copyDBResourceToDocumentPath(dbName: String?) {
//        let bundlePath1 = Bundle.init(for: self).path(forResource: "000005", ofType: "ldb")
        let bundlePath2 = Bundle.init(for: self).path(forResource: "CURRENT", ofType: nil, inDirectory: dbName ?? "PHDB")
        let bundlePath3 = Bundle.init(for: self).path(forResource: "MANIFEST-000002", ofType: nil, inDirectory: dbName ?? "PHDB")
        let bundlePath4 = Bundle.init(for: self).path(forResource: "LOCK", ofType: nil, inDirectory: dbName ?? "PHDB")
//        let bundlePath5 = Bundle.init(for: self).path(forResource: "LOG", ofType: "old")
        let bundlePath6 = Bundle.init(for: self).path(forResource: "LOG", ofType: nil, inDirectory: dbName ?? "PHDB")
        let bundlePath7 = Bundle.init(for: self).path(forResource: "000003", ofType: "log", inDirectory: dbName ?? "PHDB")


        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        var dbPath: String = ""
            
        if let _ = dbName {
            dbPath = (path as NSString).appendingPathComponent(dbName!)
        } else {
            dbPath = path
        }
        
        let fileManager = FileManager.default
        
        do {
            try fileManager.createDirectory(atPath: dbPath, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }

        
        print("********************")
        print("dbPath")
        print(dbPath)
        print("********************")
        
//        let filePath1 = URL(fileURLWithPath: dbPath).appendingPathComponent("000005.ldb", isDirectory: false)
        let filePath2 = URL(fileURLWithPath: dbPath).appendingPathComponent("CURRENT", isDirectory: false)
        let filePath3 = URL(fileURLWithPath: dbPath).appendingPathComponent("MANIFEST-000002", isDirectory: false)
        let filePath4 = URL(fileURLWithPath: dbPath).appendingPathComponent("LOCK", isDirectory: false)
//        let filePath5 = URL(fileURLWithPath: dbPath).appendingPathComponent("LOG.old", isDirectory: false)
        let filePath6 = URL(fileURLWithPath: dbPath).appendingPathComponent("LOG", isDirectory: false)
        let filePath7 = URL(fileURLWithPath: dbPath).appendingPathComponent("000003.log", isDirectory: false)


        do {
//            if fileManager.fileExists(atPath: filePath1.path) {
//                // if database already exists then no need to copy the db again and the  remaning files
//                return
//                // try fileManager.replaceItemAt(filePath1, withItemAt: URL(fileURLWithPath: bundlePath1!))
//            } else {
//                try fileManager.copyItem(atPath: bundlePath1!, toPath: filePath1.path)
//            }
            
            if fileManager.fileExists(atPath: filePath2.path) {
                try fileManager.replaceItemAt(filePath2, withItemAt: URL(fileURLWithPath: bundlePath2!))
            } else {
                try fileManager.copyItem(atPath: bundlePath2!, toPath: filePath2.path)
            }

            if fileManager.fileExists(atPath: filePath3.path) {
                try fileManager.replaceItemAt(filePath3, withItemAt: URL(fileURLWithPath: bundlePath3!))
            } else {
                try fileManager.copyItem(atPath: bundlePath3!, toPath: filePath3.path)
            }

            if fileManager.fileExists(atPath: filePath4.path) {
                try fileManager.replaceItemAt(filePath4, withItemAt: URL(fileURLWithPath: bundlePath4!))
            } else {
                try fileManager.copyItem(atPath: bundlePath4!, toPath: filePath4.path)
            }

//            if fileManager.fileExists(atPath: filePath5.path) {
//                try fileManager.replaceItemAt(filePath5, withItemAt: URL(fileURLWithPath: bundlePath5!))
//            } else {
//                try fileManager.copyItem(atPath: bundlePath5!, toPath: filePath5.path)
//            }

            if fileManager.fileExists(atPath: filePath6.path) {
                try fileManager.replaceItemAt(filePath6, withItemAt: URL(fileURLWithPath: bundlePath6!))
            } else {
                try fileManager.copyItem(atPath: bundlePath6!, toPath: filePath6.path)
            }

            if fileManager.fileExists(atPath: filePath7.path) {
                try fileManager.replaceItemAt(filePath7, withItemAt: URL(fileURLWithPath: bundlePath7!))
            } else {
                try fileManager.copyItem(atPath: bundlePath7!, toPath: filePath7.path)
            }

        } catch {
            print("copyDBResourceToDocumentPath error")
            print(error)
        }
    }
    
}
