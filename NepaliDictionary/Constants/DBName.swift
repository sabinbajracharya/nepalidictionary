//
//  DBName.swift
//  NepaliDictionary
//
//  Created by Sabin Bajracharya on 6/21/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import Foundation

public class DBName {
    public static let phDB = "PHDB"
    public static let npDB = "NPDB"
    public static let enDB = "ENDB"
}
