//
//  NepaliSuggestion.swift
//  NepaliDictionary
//
//  Created by Suraj Bhadari on 5/10/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import Foundation

public class NepaliSuggestion {
    public static var charOfH = [[String]]()
    public var r0 = [[String]]()
    public static var size = Int()
    public static var stringBuilder = [String]()
    
    public init(){
        r0.append(["क", "ख"])
        r0.append(["ग", "घ"])
        r0.append(["च", "छ"])
        r0.append(["ज", "झ"])
        r0.append(["त", "थ"])
        r0.append(["द", "ध"])
        r0.append(["ट", "ठ"])
        r0.append(["ड", "ढ"])
        r0.append(["प", "फ"])
        r0.append(["ब", "भ"])
        NepaliSuggestion.charOfH = r0
    }
    
    func isInHChar(ch: Character) -> Bool {
        var b:Bool = false
        for strArr in NepaliSuggestion.charOfH {
            let character = Array(strArr[0])
            if (ch == character[0]) {
                b = true
            }
        }
        return b
    }
    
    private static func isVowel(c: Character) -> Bool {
        let vowel:[Character] = ["ऑ", "अ", "आ", "इ", "ई", "उ", "ऊ", "ऋ", "ओ", "औ", "ए", "ऐ", "ऄ", "ऑ", "ऒ", "ऍ", "ऎ", "०", "१", "२", "३", "४", "५", "६", "७", "८", "९"]
        var b:Bool = false
        for c2:Character in vowel {
            if (c == c2) {
                b = true
            }
        }
        return b
    }
    
    private static func isMatra(c: Character) -> Bool {
        let matras:[Character] = ["ॉ", "'ं", "'ँ", "ा", "ि", "ी", "ु", "ू", "'ृ", "ो", "ौ", "'े", "'ै", "।", "·", "ॉ", "ॊ", " ँ", "'ॆ", " ँ", " ं"]
        var b:Bool = false
        for c2:Character in matras {
            if (c == c2) {
                b = true
            }
        }
        return b
    }
    
    private static func isConsonent(c: Character) -> Bool {
        let consonents:[Character] = ["क", "ख", "ग", "घ", "च", "छ", "ज", "झ", "ञ", "ट", "ठ", "ड", "ढ", "ण", "त", "थ", "द", "ध", "न", "प", "फ", "ब", "भ", "म", "य", "र", "ल", "व", "श", "ष", "स", "ह", "क़", "ख़", "ग़", "ज़", "ड़", "ढ़", "फ़", "य़"]
        var b:Bool = false
        for ch:Character in consonents {
            if (c == ch) {
                b = true
            }
        }
        return b
    }
    
    public static func wordlnt15plus(word: String) -> [String] {
        var size = Int()
        var str = String()
        var wordsAl = [String]()
        var index:Int = 0
        for i in index..<word.count {
            let characters = Array(word)
            let wordCh:Character = characters[index]
            if (wordCh == "a" || wordCh == "A") {
                if (index == 0) {
                    wordsAl.append("अ")
                } else if (index > 0) {
                }
            } else if (wordCh == "e" || wordCh == "E") {
                if (index == 0) {
                    wordsAl.append("ए")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = character + "े"
                        } else {
                            wordsAl[i] = character + "ए"
                        }
                    }
                }
            } else if (wordCh == "i" || wordCh == "I") {
                if (index == 0) {
                    wordsAl.append("ई")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str =  wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1]) && !(characters[index - 1] == "a") && (characters[index - 1] == "A")) {
                            wordsAl[i] = character + "ि"
                        }
                    }
                }
            } else if (wordCh == "o" || wordCh == "O") {
                if (index == 0) {
                    wordsAl.append("ओ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = character + "ो"
                        }
                    }
                }
            } else if (wordCh == "u" || wordCh == "U") {
                if (index == 0) {
                    wordsAl.append("ऊ")
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = character + "ौ"
                        }
                    }
                }
            } else if (wordCh == "b" || wordCh == "B") {
                if (index == 0) {
                    wordsAl.append("ब")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ब"
                    }
                }
            } else if (wordCh == "c" || wordCh == "C") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "क"
                    }
                }
            } else if (wordCh == "d" || wordCh == "D") {
                if (index == 0) {
                    wordsAl.append("द")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "द"
                    }
                }
            } else if (wordCh == "f" || wordCh == "F") {
                if (index == 0) {
                    wordsAl.append("फ")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "फ"
                    }
                }
            } else if (wordCh == "g" || wordCh == "G") {
                if (index == 0) {
                    wordsAl.append("ग")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ग"
                    }
                }
            } else if (wordCh == "h" || wordCh == "H") {
                if (index == 0) {
                    wordsAl.append("ह")
                }
            } else if (wordCh == "j" || wordCh == "J") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ज"
                    }
                }
            } else if (wordCh == "k" || wordCh == "K") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "क"
                    }
                }
            } else if (wordCh == "l" || wordCh == "L") {
                if (index == 0) {
                    wordsAl.append("ल")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ल"
                    }
                }
            } else if (wordCh == "m" || wordCh == "M") {
                if (index == 0) {
                    wordsAl.append("म")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "म"
                    }
                }
            } else if (wordCh == "n" || wordCh == "N") {
                if (index == 0) {
                    wordsAl.append("न")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "न"
                    }
                }
            } else if (wordCh == "p" || wordCh == "P") {
                if (index == 0) {
                    wordsAl.append("प")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "प"
                    }
                }
            } else if (wordCh == "q" || wordCh == "Q") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "क"
                    }
                }
            } else if (wordCh == "r" || wordCh == "R") {
                if (index == 0) {
                    wordsAl.append("र")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "र"
                    }
                }
            } else if (wordCh == "s" || wordCh == "S") {
                if (index == 0) {
                    wordsAl.append("स")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "स"
                    }
                }
            } else if (wordCh == "t" || wordCh == "T") {
                if (index == 0) {
                    wordsAl.append("त")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "त"
                    }
                }
            } else if (wordCh == "v" || wordCh == "V") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl.append((wordsAl[i]) + "व")
                    }
                }
            } else if (wordCh == "w" || wordCh == "W") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "व"
                    }
                }
            } else if (wordCh == "x" || wordCh == "X") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ज"
                    }
                }
            } else if (wordCh == "y" || wordCh == "Y") {
                if (index == 0) {
                    wordsAl.append("य")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "य"
                    }
                }
            } else if (wordCh == "z" || wordCh == "Z") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + "ज"
                    }
                }
            } else if (wordCh == "1" || wordCh == "2" || wordCh == "3" || wordCh == "4" || wordCh == "5" || wordCh == "6" || wordCh == "7" || wordCh == "8" || wordCh == "9" || wordCh == "0" || wordCh == "~" || wordCh == "`" || wordCh == "!" || wordCh == "@" || wordCh == "#" || wordCh == "$" || wordCh == "%" || wordCh == "^" || wordCh == "&" || wordCh == "*" || wordCh == "(" || wordCh == ")" || wordCh == "_" || wordCh == "-" || wordCh == "+" || wordCh == "=" || wordCh == "{" || wordCh == "}" || wordCh == "[" || wordCh == "]" || wordCh == nil || wordCh == ":" || wordCh == "\"" || wordCh == "\"" || wordCh == "<" || wordCh == "," || wordCh == ">" || wordCh == "." || wordCh == "?" || wordCh == "/") {
                size = wordsAl.count
                for i in 0..<size {
                    wordsAl[i] = wordsAl[i] + String(wordCh)
                }
            }
            index = index + 1
        }
        return removeDuplicate(arrayList: wordsAl)
    }
    
    public static func mapEngHin_S(word:String) -> [String] {
        var wordsAl = [String]()
        var index:Int = 0
        while (index < word.count) {
            let characters = Array(word)
            let wordCh:Character = characters[index]
            var size = Int()
            var str = String()
            if (wordCh == "a" || wordCh == "A") {
                if (index == 0) {
                    wordsAl.append("अ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count == 1 && character[0] == "अ" && ((characters[index - 1] == "a" || characters[index - 1] == "A") && (characters[index] == "a" || characters[index] == "A"))) {
                            wordsAl[i] =  String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "आ"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ा"
                            wordsAl.append(str)
                        } else if (!isVowel(c: character[str.count - 1]) && isMatra(c: character[str.count - 1])) {
                        }
                    }
                }
            } else if (wordCh == "e" || wordCh == "E") {
                if (index == 0) {
                    wordsAl.append("ए")
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ए"
                        } else if (isMatra(c: character[str.count - 1])) {
                            if ((characters[index - 1] != "e" && characters[index - 1] != "E") || (characters[index] != "e" && characters[index] != "E")) {
                                wordsAl[i] = str + "ए"
                            } else if (character[str.count - 1] == "ए") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ी"
                            }
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ए"
                        }
                    }
                }
            } else if (wordCh == "i" || wordCh == "I") {
                if (index == 0) {
                    wordsAl.append("इ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "र") {
                            wordsAl[i] = str + "ि"
                        } else if (index == 1 && ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "अ")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऐ"
                        } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ै"
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] == "a" && characters[index - 1] == "A") {
                                wordsAl[i] = str + "ई"
                            } else {
                                wordsAl[i] = str + "ि"
                            }
                        }
                    }
                }
            } else if (wordCh == "o" || wordCh == "O") {
                if (index == 0) {
                    wordsAl.append("ओ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if ((characters[index] == "o" || characters[index] == "O") && ((characters[index - 1] == "o" || characters[index - 1] == "O") && character[str.count - 1] == "ो")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ू"
                        } else if (isConsonent(c: character[str.count - 1]) || character[str.count - 1] == "़") {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ओ"
                            } else {
                                wordsAl[i] = str + "ो"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        }
                    }
                }
            } else if (wordCh == "u" || wordCh == "U") {
                if (index == 0) {
                    wordsAl.append("ऊ")
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                        } else if (index == 1 && ((character[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "अ")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                        } else if (isConsonent(c:character[str.count - 1])) {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "उ"
                            } else {
                                wordsAl[i] = str + "ू"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ऊ"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ऊ"
                        }
                    }
                }
            } else if (wordCh == "b" || wordCh == "B") {
                if (index == 0) {
                    wordsAl.append("ब")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ब"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "ब"
                        } else {
                            wordsAl.append(str + "्ब")
                        }
                    }
                }
            } else if (wordCh == "c" || wordCh == "C") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "क"
                        } else {
                            wordsAl.append(str + "्क")
                        }
                    }
                }
            } else if (wordCh == "d" || wordCh == "D") {
                if (index == 0) {
                    wordsAl.append("द")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "द"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "द"
                        } else {
                            wordsAl.append(str + "्द")
                        }
                    }
                }
            } else if (wordCh == "f" || wordCh == "F") {
                if (index == 0) {
                    wordsAl.append("फ")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "फ"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "फ"
                        } else {
                            wordsAl.append(str + "्फ")
                        }
                    }
                }
            } else if (wordCh == "g" || wordCh == "G") {
                if (index == 0) {
                    wordsAl.append("ग")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ग"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "ग"
                        } else {
                            wordsAl.append(str + "्ग")
                        }
                    }
                }
            } else if (wordCh == "h" || wordCh == "H") {
                if (index == 0) {
                    wordsAl.append("ह")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (characters[index - 1] == "f" || characters[index - 1] == "F" || characters[index - 1] == "r" || characters[index - 1] == "R" || characters[index - 1] == "m" || characters[index - 1] == "M" || characters[index - 1] == "l" || characters[index - 1] == "L" || characters[index - 1] == "n" || characters[index - 1] == "N" || characters[index - 1] == "a" || characters[index - 1] == "A" || characters[index - 1] == "e" || characters[index - 1] == "E" || characters[index - 1] == "i" || characters[index - 1] == "I" || characters[index - 1] == "o" || characters[index - 1] == "O" || characters[index - 1] == "u" || characters[index - 1] == "U" || characters[index - 1] == "v" || characters[index - 1] == "V") {
                            if (str.count <= 0 || character[str.count - 1] == "़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ह"
                            } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ह"
                            } else {
                                wordsAl.append(str + "्ह")
                            }
                        } else if (index > 0 && characters[index - 1] != "c" && characters[index - 1] != "C" && characters[index - 1] != "a" && characters[index - 1] != "A" && str.count > 0 && (character[str.count - 1] == "क" || character[str.count - 1] == "ग" || character[str.count - 1] == "च" || character[str.count - 1] == "ज" || character[str.count - 1] == "ट" || character[str.count - 1] == "ड" || character[str.count - 1] == "त" || character[str.count - 1] == "द" || character[str.count - 1] == "प" || character[str.count - 1] == "ब")) {
                            if (characters[index - 1] == "p" || characters[index - 1] == "P") {
                                wordsAl.append(str + "ह")
                            }
                            for j in 0..<charOfH.count {
                                if (character[str.count - 1] == Array(charOfH[j][0])[0]) {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + String(Array(charOfH[j][1])[0])
                                    if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]) || (characters[index - 1] == "a" && characters[index - 1] == "A"))) {
                                        wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + charOfH[j][1])
                                    }
                                }
                            }
                        } else if (characters[index - 1] == "c" || characters[index - 1] == "C") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "च"
                        } else if (character[str.count - 1] == "स") {
                            //System.out.println("kunwaravanish out this look of charof h")
                            if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]))) {
                                wordsAl[i] = str + "्" + String(character[str.count - 1])
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श")
                                }
                            }
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श"
                        }
                    }
                }
            } else if (wordCh == "j" || wordCh == "J") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || character[str.count - 1] == "़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ज"
                        } else if (character[index - 1] == "a" || character[index - 1] == "A") {
                            wordsAl[i] = str + "ज"
                        } else {
                            wordsAl.append(str + "्ज")
                        }
                    }
                }
            } else if (wordCh == "k" || wordCh == "K") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "क"
                        } else if (character[index - 1] == "a" || character[index - 1] == "A") {
                            wordsAl[i] = str + "क"
                        } else {
                            wordsAl.append(str + "्क")
                        }
                    }
                }
            } else if (wordCh == "l" || wordCh == "L") {
                if (index == 0) {
                    wordsAl.append("ल")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ल"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "ल"
                        } else {
                            wordsAl.append(str + "्ल")
                        }
                    }
                }
            } else if (wordCh == "m" || wordCh == "M") {
                if (index == 0) {
                    wordsAl.append("म")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "म"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "म"
                        } else {
                            wordsAl.append(str + "्म")
                        }
                    }
                }
            } else if (wordCh == "n" || wordCh == "N") {
                if (index == 0) {
                    wordsAl.append("न")
                } else {
                    size = wordsAl.count
                    var al = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "न"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "न"
                        } else {
                            wordsAl.append(str + "्न")
                        }
                    }
                }
            } else if (wordCh == "p" || wordCh == "P") {
                if (index == 0) {
                    wordsAl.append("प")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "प"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "प"
                        } else {
                            wordsAl.append(str + "्प")
                        }
                    }
                }
            } else if (wordCh == "q" || wordCh == "Q") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "क"
                        } else {
                            wordsAl.append(str + "्क")
                        }
                    }
                }
            } else if (wordCh == "r" || wordCh == "R") {
                if (index == 0) {
                    wordsAl.append("र")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "र"
                        } else if (character[index - 1] == "a" || character[index - 1] == "A") {
                            wordsAl[i] = str + "र"
                        } else {
                            wordsAl.append(str + "्र")
                        }
                    }
                }
            }else if (wordCh == "s" || wordCh == "S") {
                if (index == 0) {
                    wordsAl.append("स")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "स"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "स"
                        } else {
                            wordsAl.append(str + "्स")
                        }
                    }
                }
            } else if (wordCh == "t" || wordCh == "T") {
                if (index == 0) {
                    wordsAl.append("त")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "त"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "त"
                        } else {
                            wordsAl.append(str + "्त")
                        }
                    }
                }
            } else if (wordCh == "v" || wordCh == "V") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "व"
                        } else {
                            wordsAl.append(str + "्व")
                        }
                    }
                }
            } else if (wordCh == "w" || wordCh == "W") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "व"
                        } else {
                            wordsAl.append(str + "्व")
                        }
                    }
                }
            } else if (wordCh == "x" || wordCh == "X") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ज"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "ज"
                        } else {
                            wordsAl.append(str + "्ज")
                        }
                    }
                }
            } else if (wordCh == "y" || wordCh == "Y") {
                if (index == 0) {
                    wordsAl.append("य")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "य"
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            }
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्य")
                            }
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            } else {
                                wordsAl[i] = str + "य"
                            }
                        }
                    }
                }
            } else if (wordCh == "z" || wordCh == "Z") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ज"
                        } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                            wordsAl[i] = str + "ज"
                        } else {
                            wordsAl.append(str + "्ज")
                        }
                    }
                }
            } else if (wordCh == "1" || wordCh == "2" || wordCh == "3" || wordCh == "4" || wordCh == "5" || wordCh == "6" || wordCh == "7" || wordCh == "8" || wordCh == "9" || wordCh == "0" || wordCh == "~" || wordCh == "`" || wordCh == "!" || wordCh == "@" || wordCh == "#" || wordCh == "$" || wordCh == "%" || wordCh == "^" || wordCh == "&" || wordCh == "*" || wordCh == "(" || wordCh == ")" || wordCh == "_" || wordCh == "-" || wordCh == "+" || wordCh == "=" || wordCh == "{" || wordCh == "}" || wordCh == "[" || wordCh == "]" || wordCh == nil || wordCh == ":" || wordCh == "\"" || wordCh == "\"" || wordCh == "<" || wordCh == "," || wordCh == ">" || wordCh == "." || wordCh == "?" || wordCh == "/") {
                size = wordsAl.count
                for i in 0..<size {
                    wordsAl[i] = wordsAl[i] + String(wordCh)
                }
            }
            index = index + 1
        }
        //        var nonRedundantLists:[String] = removeDuplicate(arrayList: wordsAl)
        /*  if(nonRedundantLists.count>0){
         nonRedundantLists.get(0)
         }*/
        return removeDuplicate(arrayList: wordsAl)
    }
    
    
    //    public static func ReadFromfile(fileName:String) -> String {
    //    StringBuilder returnString = new StringBuilder()
    //    InputStream fIn = null
    //    InputStreamReader isr = null
    //    BufferedReader input = null
    //    try {
    //    fIn = context.getResources().getAssets().open(fileName, Context.MODE_WORLD_READABLE)
    //    isr = new InputStreamReader(fIn)
    //    input = new BufferedReader(isr)
    //    String line = ""
    //    while ((line = input.readLine()) != null) {
    //    returnString.append(line)
    //    }
    //    } catch (Exception e) {
    //    e.getMessage()
    //    } finally {
    //    try {
    //    if (isr != null)
    //    isr.close()
    //    if (fIn != null)
    //    fIn.close()
    //    if (input != null)
    //    input.close()
    //    } catch (Exception e2) {
    //    e2.getMessage()
    //    }
    //    }
    //    return returnString.toString()
    //    }
    
    
    
    public static func mapEngNep_L( word: String) -> [String] {
        var wordsAl = [String]()
        var index:Int = 0
        let characters = Array(word)
        while (index < word.count) {
            let wordCh:Character = characters[index]
            var size = Int()
            var str = String()
            if (wordCh == "a" || wordCh == "A") {
                if (index == 0) {
                    wordsAl.append("अ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count == 1 && character[0] == "अ" && ((characters[index - 1] == "a" || characters[index - 1] == "A") && (characters[index] == "a" || characters[index] == "A"))) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "आ"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ा"
                            wordsAl.append(str)
                        } else if (!isVowel(c: character[str.count - 1]) && isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ा"
                            //wordsAl.append(str)
                        }
                    }
                }
            } else if (wordCh == "e" || wordCh == "E") {
                if (index == 0) {
                    wordsAl.append("ए")
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ए"
                            } else {
                                wordsAl[i] = str + "े"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            if ((characters[index - 1] != "e" && characters[index - 1] != "E") || (characters[index] != "e" && characters[index] != "E")) {
                                wordsAl[i] = str + "ए"
                            } else if (character[str.count - 1] == "े") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ी"
                            }
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ए"
                        }
                    }
                }
            } else if (wordCh == "i" || wordCh == "I") {
                if (index == 0) {
                    wordsAl.append("इ")
                } else if (index > 0) {
                    size = wordsAl.count
                    var al = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "र") {
                            wordsAl[i] = str + "ि"
                            if (str.count == 1) {
                                al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऋ")
                            }
                            if (str.count > 1 && (character[str.count - 2] == "स" || character[str.count - 2] == "ह" || character[str.count - 2] == "क" || character[str.count - 2] == "ग" || character[str.count - 2] == "घ" || character[str.count - 2] == "त" || character[str.count - 2] == "द" || character[str.count - 2] == "न" || character[str.count - 2] == "प" || character[str.count - 2] == "म" || character[str.count - 2] == "व" || character[str.count - 2] == "ष")) {
                                al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ृ")
                            }
                        } else if (index == 1 && ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "अ")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऐ"
                        } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ै"
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] == "a" && characters[index - 1] == "A") {
                                wordsAl[i] = str + "ई"
                            } else {
                                wordsAl[i] = str + "ि"
                            }
                        }
                    }
                    for i in 0..<al.count {
                        wordsAl.append(al[i])
                    }
                }
            } else if (wordCh == "o" || wordCh == "O") {
                if (index == 0) {
                    wordsAl.append("ओ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if ((characters[index] == "o" || characters[index] == "O") && ((characters[index - 1] == "o" || characters[index - 1] == "O") && character[str.count - 1] == "ो")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ू"
                        } else if (isConsonent(c: character[str.count - 1]) || character[str.count - 1] == "़") {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ओ"
                            } else {
                                wordsAl[i] = str + "ो"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        }
                    }
                }
            } else if (wordCh == "u" || wordCh == "U") {
                if (index == 0) {
                    wordsAl.append("ऊ")
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                        } else if (index == 1 && ((characters[index - 1] == "a" || characters[index - 1] == "A") && characters[str.count - 1] == "अ")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "उ"
                            } else {
                                wordsAl[i] = str + "ू"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            if (character[str.count - 1] == "ौ") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                            } else {
                                wordsAl[i] = str + "ऊ"
                            }
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ऊ"
                        }
                    }
                }
            } else if (wordCh == "b" || wordCh == "B") {
                if (index == 0) {
                    wordsAl.append("ब")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ब"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ब")
                            }
                            wordsAl[i] = str + "ब"
                        }
                    }
                }
            } else if (wordCh == "c" || wordCh == "C") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "d" || wordCh == "D") {
                if (index == 0) {
                    wordsAl.append("द")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "द"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्द")
                            }
                            wordsAl[i] = str + "द"
                        }
                    }
                }
            } else if (wordCh == "f" || wordCh == "F") {
                if (index == 0) {
                    wordsAl.append("फ")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "फ"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्फ")
                            }
                            wordsAl[i] = str + "फ"
                        }
                    }
                }
            } else if (wordCh == "g" || wordCh == "G") {
                if (index == 0) {
                    wordsAl.append("ग")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ग"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ग")
                            }
                            wordsAl[i] = str + "ग"
                        }
                    }
                }
            } else if (wordCh == "h" || wordCh == "H") {
                if (index == 0) {
                    wordsAl.append("ह")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (characters[index - 1] == "f" || characters[index - 1] == "F" || characters[index - 1] == "r" || characters[index - 1] == "R" || characters[index - 1] == "m" || characters[index - 1] == "M" || characters[index - 1] == "l" || characters[index - 1] == "L" || characters[index - 1] == "n" || characters[index - 1] == "N" || characters[index - 1] == "a" || characters[index - 1] == "A" || characters[index - 1] == "e" || characters[index - 1] == "E" || characters[index - 1] == "i" || characters[index - 1] == "I" || characters[index - 1] == "o" || characters[index - 1] == "O" || characters[index - 1] == "u" || characters[index - 1] == "U" || characters[index - 1] == "v" || characters[index - 1] == "V") {
                            if (str.count <= 0 || character[str.count - 1] == "'़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ह"
                            } else {
                                wordsAl[i] = str + "ह"
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ह")
                                }
                            }
                        } else if (index > 0 && characters[index - 1] != "c" && characters[index - 1] != "C" && characters[index - 1] != "a" && characters[index - 1] != "A" && str.count > 0 && (character[str.count - 1] == "क" || character[str.count - 1] == "ग" || character[str.count - 1] == "च" || character[str.count - 1] == "ज" || character[str.count - 1] == "ट" || character[str.count - 1] == "ड" || character[str.count - 1] == "त" || character[str.count - 1] == "द" || character[str.count - 1] == "प" || character[str.count - 1] == "ब")) {
                            if (characters[index - 1] == "p" || characters[index - 1] == "P") {
                                wordsAl.append(str + "ह")
                            }
                            for j in 0..<charOfH.count {
                                if (character[str.count - 1] == Array(charOfH[j][0])[0]) {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + String(Array(charOfH[j][1])[0])
                                    //System.out.println("ksh word" + str.substring(0, str.count - 1) + charOfH[j][1].charAt(0))
                                    if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]) || (characters[index - 1] == "a" && characters[index - 1] == "A"))) {
                                        wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + charOfH[j][1])
                                    }
                                }
                            }
                        } else if (characters[index - 1] == "c" || characters[index - 1] == "C") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "च"
                        } else if (character[str.count - 1] == "स") {
                            //System.out.println("kunwaravanish out this look of charof h")
                            if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]))) {
                                wordsAl[i] = str + "्" + String(character[str.count - 1])
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श")
                                }
                            }
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श"
                        }
                    }
                }
            } else if (wordCh == "j" || wordCh == "J") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || character[str.count - 1] == "़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ज"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                            }
                            wordsAl[i] = str + "ज"
                        }
                    }
                }
            } else if (wordCh == "k" || wordCh == "K") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "l" || wordCh == "L") {
                if (index == 0) {
                    wordsAl.append("ल")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ल"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ल")
                            }
                            wordsAl[i] = str + "ल"
                        }
                    }
                }
            } else if (wordCh == "m" || wordCh == "M") {
                if (index == 0) {
                    wordsAl.append("म")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "म"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्म")
                            }
                            wordsAl[i] = str + "म"
                        }
                    }
                }
            } else if (wordCh == "n" || wordCh == "N") {
                if (index == 0) {
                    wordsAl.append("न")
                } else {
                    size = wordsAl.count
                    var arrayList = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "न"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्न")
                            }
                            wordsAl[i] = str + "न"
                        }
                    }
                }
            } else if (wordCh == "p" || wordCh == "P") {
                if (index == 0) {
                    wordsAl.append("प")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "प"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्प")
                            }
                            wordsAl[i] = str + "प"
                        }
                    }
                }
            } else if (wordCh == "q" || wordCh == "Q") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "r" || wordCh == "R") {
                if (index == 0) {
                    wordsAl.append("र")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "र"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्र")
                            }
                            wordsAl[i] = str + "र"
                        }
                    }
                }
            } else if (wordCh == "s" || wordCh == "S") {
                if (index == 0) {
                    wordsAl.append("स")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "स"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्स")
                            }
                            wordsAl[i] = str + "स"
                        }
                    }
                }
            } else if (wordCh == "t" || wordCh == "T") {
                if (index == 0) {
                    wordsAl.append("त")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "त"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्त")
                            }
                            wordsAl[i] = str + "त"
                        }
                    }
                }
            } else if (wordCh == "v" || wordCh == "V") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्व")
                            }
                            wordsAl[i] = str + "व"
                        }
                    }
                }
            } else if (wordCh == "w" || wordCh == "W") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्व")
                            }
                            wordsAl[i] = str + "व"
                        }
                    }
                }
            } else if (wordCh == "x" || wordCh == "X") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ज"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                            }
                            wordsAl[i] = str + "ज"
                        }
                    }
                }
            } else if (wordCh == "y" || wordCh == "Y") {
                if (index == 0) {
                    wordsAl.append("य")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "य"
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            }
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्य")
                            }
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            }
                            wordsAl[i] = str + "य"
                        }
                    }
                }
            } else if (wordCh == "z" || wordCh == "Z") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ज"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                            }
                            wordsAl[i] = str + "ज"
                        }
                    }
                }
            } else if (wordCh == "1" || wordCh == "2" || wordCh == "3" || wordCh == "4" || wordCh == "5" || wordCh == "6" || wordCh == "7" || wordCh == "8" || wordCh == "9" || wordCh == "0" || wordCh == "~" || wordCh == "`" || wordCh == "!" || wordCh == "@" || wordCh == "#" || wordCh == "$" || wordCh == "%" || wordCh == "^" || wordCh == "&" || wordCh == "*" || wordCh == "(" || wordCh == ")" || wordCh == "_" || wordCh == "-" || wordCh == "+" || wordCh == "=" || wordCh == "{" || wordCh == "}" || wordCh == "[" || wordCh == "]" || wordCh == nil || wordCh == ":" || wordCh == "\"" || wordCh == "\"" || wordCh == "<" || wordCh == "," || wordCh == ">" || wordCh == "." || wordCh == "?" || wordCh == "/") {
                size = wordsAl.count
                for i in 0..<size {
                    wordsAl[i] = wordsAl[i] + String(wordCh)
                }
            }
            index = index + 1
        }
        return removeDuplicate(arrayList: wordsAl)
    }
    
    public static func mapEngHndCom(word: String, test: NPDictionary) -> [String] {
        let suggestions:[String] = test.getSuggestions(string: word)
        
        if (suggestions.count > 0) {
            return suggestions
        }
        let characters = Array(word)
        var wordsAl = [String]()
        var index:Int = 0
        while (index < word.count) {
            let wordCh:Character = characters[index]
            var size = Int()
            var str = String()
            if (wordCh == "a" || wordCh == "A") {
                if (index == 0) {
                    wordsAl.append("अ")
                    wordsAl.append("आ")
                } else if (index > 0) {
                    if (index < word.count - 1 && ((characters[index] == "a" || characters[index] == "A") && (characters[index + 1] == "a" || characters[index + 1] == "A"))) {
                        //index++
                        
                    }
                    size = wordsAl.count
                    var v = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count != 1 || character[0] != "आ") {
                            if (isConsonent(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ा"
                                v.append(str)
                            } else if (!isVowel(c: character[str.count - 1]) && isMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "आ"
                            }
                        }
                    }
                    if (v.count > 0) {
                        for i in 0..<v.count {
                            wordsAl.append(v[i])
                        }
                    }
                }
            } else if (wordCh == "e" || wordCh == "E") {
                if (index == 0) {
                    if (index >= word.count - 1 || !((characters[index] == "e" || characters[index] == "E") && (characters[index + 1] == "e" || characters[index + 1] == "E"))) {
                        wordsAl.append("ए")
                        wordsAl.append("इ")
                    } else {
                        index = index + 1
                        wordsAl.append("ए")
                    }
                } else if (index > 0) {
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (index == 1 && ((characters[index - 1] == "e" || characters[index - 1] == "E") && (characters[index] == "e" || characters[index] == "E"))) {
                            if (character[str.count - 1] == "ए") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ई"
                            }
                        } else if (isConsonent(c: character[str.count - 1])) {
                            wordsAl[i] = str + "े"
                        } else if (isMatra(c: character[str.count - 1])) {
                            if (word.count <= 1 || !((characters[index - 2] == "e" || characters[index - 2] == "E") && ((characters[index - 1] == "e" || characters[index - 1] == "E") && (characters[index] == "e" || characters[index] == "E")))) {
                                if ((characters[index - 1] != "e" && characters[index - 1] != "E") || (characters[index] != "e" && characters[index] != "E")) {
                                    wordsAl[i] = str + "ए"
                                } else if (character[str.count - 1] == "े") {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ी"
                                }
                            } else if (character[str.count - 1] == "ी") {
                                wordsAl[i] = str + "ए"
                            }
                        } else if (isVowel(c: character[str.count - 1])) {
                            if ((characters[index - 1] != "e" && characters[index - 1] != "E") || (characters[index] != "e" && characters[index] != "E")) {
                                wordsAl[i] = str + "ए"
                            } else if (character[str.count - 1] == "ए") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ई"
                            }
                        }
                    }
                }
            } else if (wordCh == "i" || wordCh == "I") {
                if (index == 0) {
                    wordsAl.append("इ")
                    wordsAl.append("ई")
                } else if (index > 0) {
                    size = wordsAl.count
                    var al = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "र") {
                            wordsAl[i] = str + "ि"
                            wordsAl.append(str + "ी")
                            if (str.count == 1) {
                                al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऋ")
                            }
                            if (str.count > 1 && (character[str.count - 2] == "स" || character[str.count - 2] == "ह" || character[str.count - 2] == "क" || character[str.count - 2] == "ग" || character[str.count - 2] == "घ" || character[str.count - 2] == "त" || character[str.count - 2] == "द" || character[str.count - 2] == "न" || character[str.count - 2] == "प" || character[str.count - 2] == "म" || character[str.count - 2] == "व" || character[str.count - 2] == "ष")) {
                                al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ृ")
                            }
                        } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ै"
                            al.append(str + "ई")
                            al.append(str + "इ")
                        } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "आ") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऐ"
                        } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "अ") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ए"
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                            al.append(str + "ई")
                            al.append(str + "इ")
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ई"
                            al.append(str + "इ")
                        } else if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] != "a" || characters[index - 1] != "A") {
                                wordsAl[i] = str + "ि"
                                wordsAl.append(str + "ी")
                            } else if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ई"
                            }
                        } else if (word.count > 1 && ((characters[word.count - 2] == "i" || characters[word.count - 2] == "I") && isVowel(c: character[str.count - 1]))) {
                            wordsAl[i] = str + "ई"
                        } else if (word.count > 1 && ((characters[word.count - 2] == "i" || characters[word.count - 2] == "I") && isMatra(c: character[str.count - 1]))) {
                            wordsAl[i] = str + "ई"
                        }
                    }
                    for i in 0..<al.count {
                        wordsAl.append(al[i])
                    }
                }
            } else if (wordCh == "o" || wordCh == "O") {
                if (index == 0) {
                    wordsAl.append("ओ")
                    wordsAl.append("ऑ")
                } else if (index > 0) {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if ((characters[index] == "o" || characters[index] == "O") && ((characters[index - 1] == "o" || characters[index - 1] == "O") && character[str.count - 1] == "ओ")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                        } else if ((characters[index] == "o" || characters[index] == "O") && ((characters[index - 1] == "o" || characters[index - 1] == "O") && character[str.count - 1] == "ो")) {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ु"
                        } else if (isConsonent(c: character[str.count - 1]) || character[str.count - 1] == "़") {
                            if (characters[index - 1] != "a" && characters[index - 1] != "A") {
                                wordsAl[i] = str + "ो"
                                wordsAl.append(str + "ॉ")
                            } else if (str.count > 1 && character[str.count - 1] == "'़" && (characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl[i] = str + "ओ"
                            } else {
                                wordsAl[i] = str + "ओ"
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        } else if (isVowel(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ओ"
                        }
                    }
                }
            } else if (wordCh == "u" || wordCh == "U") {
                if (index == 0) {
                    wordsAl.append("उ")
                    wordsAl.append("ऊ")
                } else if (index > 0) {
                    var al = [String]()
                    for i in 0..<wordsAl.count {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (character[str.count - 1] == "ा") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                            al.append(str + "ऊ")
                        } else if (character[str.count - 1] == "अ") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ओ"
                        } else if (character[str.count - 1] == "आ") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                            al.append(str + "ऊ")
                        } else if (character[str.count - 1] == "्") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऊ"
                        } else if (isConsonent(c: character[str.count - 1])) {
                            if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                wordsAl[i] = str + "ऊ"
                                al.append(str + "उ")
                            } else {
                                wordsAl[i] = str + "ू"
                                al.append(str + "ु")
                            }
                        } else if (isMatra(c: character[str.count - 1])) {
                            if (character[str.count - 1] == "ौ") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                            } else {
                                wordsAl[i] = str + "ऊ"
                            }
                            al.append(str + "उ")
                        } else if (isVowel(c: character[str.count - 1])) {
                            if (character[str.count - 1] == "ओ") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                            }
                            al.append(str + "ऊ")
                        }
                    }
                    for i in 0..<al.count {
                        wordsAl.append(al[i])
                    }
                }
            } else if (wordCh == "b" || wordCh == "B") {
                if (index == 0) {
                    wordsAl.append("ब")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ब"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ब")
                            }
                            wordsAl[i] = str + "ब"
                        }
                    }
                }
            } else if (wordCh == "c" || wordCh == "C") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "d" || wordCh == "D") {
                if (index == 0) {
                    wordsAl.append("द")
                    wordsAl.append("ड")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl.append(str + "द")
                            wordsAl[i] = str + "ड"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्द")
                                wordsAl.append(str + "्ड")
                            }
                            wordsAl[i] = str + "द"
                            wordsAl.append(str + "ड")
                        }
                    }
                }
            } else if (wordCh == "f" || wordCh == "F") {
                if (index == 0) {
                    wordsAl.append("फ")
                    wordsAl.append("फ़")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "फ"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्फ")
                            }
                            wordsAl[i] = str + "फ"
                        }
                    }
                }
            } else if (wordCh == "g" || wordCh == "G") {
                if (index == 0) {
                    wordsAl.append("ग")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ग"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ग")
                            }
                            wordsAl[i] = str + "ग"
                        }
                    }
                }
            } else if (wordCh == "h" || wordCh == "H") {
                if (index == 0) {
                    wordsAl.append("ह")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (characters[index - 1] == "f" || characters[index - 1] == "F" || characters[index - 1] == "r" || characters[index - 1] == "R" || characters[index - 1] == "m" || characters[index - 1] == "M" || characters[index - 1] == "l" || characters[index - 1] == "L" || characters[index - 1] == "n" || characters[index - 1] == "N" || characters[index - 1] == "a" || characters[index - 1] == "A" || characters[index - 1] == "e" || characters[index - 1] == "E" || characters[index - 1] == "i" || characters[index - 1] == "I" || characters[index - 1] == "o" || characters[index - 1] == "O" || characters[index - 1] == "u" || characters[index - 1] == "U" || characters[index - 1] == "v" || characters[index - 1] == "V") {
                            if (str.count <= 0 || character[str.count - 1] == "'़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ह"
                            } else {
                                wordsAl[i] = str + "ह"
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ह")
                                }
                            }
                        } else if (index > 0 && characters[index - 1] != "c" && characters[index - 1] != "C" && characters[index - 1] != "a" && characters[index - 1] != "A" && str.count > 0 && (character[str.count - 1] == "क" || character[str.count - 1] == "ग" || character[str.count - 1] == "च" || character[str.count - 1] == "ज" || character[str.count - 1] == "ट" || character[str.count - 1] == "ड" || character[str.count - 1] == "त" || character[str.count - 1] == "द" || character[str.count - 1] == "प" || character[str.count - 1] == "ब")) {
                            if (characters[index - 1] == "p" || characters[index - 1] == "P") {
                                wordsAl.append(str + "ह")
                            }
                            for j in 0..<charOfH.count {
                                if (character[str.count - 1] == Array(charOfH[j][0])[0]) {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + String(Array(charOfH[j][1])[0])
                                    if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]) || (characters[index - 1] == "a" && characters[index - 1] == "A"))) {
                                        wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + charOfH[j][1])
                                    }
                                }
                            }
                        } else if (characters[index - 1] == "c" || characters[index - 1] == "C") {
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "च"
                        } else if (character[str.count - 1] == "स") {
                            if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]))) {
                                wordsAl[i] = str + "्" + String(character[str.count - 1])
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श")
                                }
                            }
                            wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श"
                            if (!(str.count <= 1 || isVowel(c: character[str.count - 2]) || isMatra(c: character[str.count - 2]) || characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ष")
                            }
                        }
                    }
                }
            } else if (wordCh == "j" || wordCh == "J") {
                if (index == 0) {
                    wordsAl.append("ज")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || character[str.count - 1] == "़" || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ज"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                            }
                            wordsAl[i] = str + "ज"
                        }
                    }
                }
            } else if (wordCh == "k" || wordCh == "K") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "l" || wordCh == "L") {
                if (index == 0) {
                    wordsAl.append("ल")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "ल"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ल")
                            }
                            wordsAl[i] = str + "ल"
                        }
                    }
                }
            } else if (wordCh == "m" || wordCh == "M") {
                if (index == 0) {
                    wordsAl.append("म")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1])) {
                            wordsAl[i] = str + "म"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्म")
                            }
                            wordsAl[i] = str + "म"
                        }
                    }
                }
            } else if (wordCh == "n" || wordCh == "N") {
                if (index == 0) {
                    wordsAl.append("न")
                } else {
                    size = wordsAl.count
                    var al = [String]()
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "न"
                            wordsAl.append(str + "ं")
                            al.append(str + "ण")
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्न")
                                wordsAl.append(str + "ं")
                                al.append(str + "ण")
                            }
                            wordsAl[i] = str + "न"
                            wordsAl.append(str + "ं")
                            al.append(str + "ण")
                        }
                    }
                    for i in 0..<al.count {
                        wordsAl.append(al[i])
                    }
                }
            } else if (wordCh == "p" || wordCh == "P") {
                if (index == 0) {
                    wordsAl.append("प")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "प"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्प")
                            }
                            wordsAl[i] = str + "प"
                        }
                    }
                }
            } else if (wordCh == "q" || wordCh == "Q") {
                if (index == 0) {
                    wordsAl.append("क")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "क"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्क")
                            }
                            wordsAl[i] = str + "क"
                        }
                    }
                }
            } else if (wordCh == "r" || wordCh == "R") {
                if (index == 0) {
                    wordsAl.append("र")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "र"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्र")
                            }
                            wordsAl[i] = str + "र"
                        }
                    }
                }
            } else if (wordCh == "s" || wordCh == "S") {
                if (index == 0) {
                    wordsAl.append("स")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "स"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्स")
                            }
                            wordsAl[i] = str + "स"
                        }
                    }
                }
            } else if (wordCh == "t" || wordCh == "T") {
                if (index == 0) {
                    wordsAl.append("त")
                    wordsAl.append("ट")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl.append(str + "ट")
                            wordsAl[i] = str + "त"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ट")
                                wordsAl.append(str + "्त")
                            }
                            wordsAl.append(str + "त")
                            wordsAl[i] = str + "ट"
                        }
                    }
                }
            } else if (wordCh == "v" || wordCh == "V") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्व")
                            }
                            wordsAl[i] = str + "व"
                        }
                    }
                }
            } else if (wordCh == "w" || wordCh == "W") {
                if (index == 0) {
                    wordsAl.append("व")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "व"
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्व")
                            }
                            wordsAl[i] = str + "व"
                        }
                    }
                }
            } else if (wordCh == "x" || wordCh == "X") {
                if (index == 0) {
                    wordsAl.append("ज")
                    wordsAl.append("क्ष")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "ज"
                            wordsAl.append(str + "क्ष")
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                            }
                            wordsAl[i] = str + "ज"
                        }
                    }
                }
            } else if (wordCh == "y" || wordCh == "Y") {
                if (index == 0) {
                    wordsAl.append("य")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                            wordsAl[i] = str + "य"
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            }
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्य")
                            }
                            if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                            }
                            wordsAl[i] = str + "य"
                        }
                    }
                }
            } else if (wordCh == "z" || wordCh == "Z") {
                if (index == 0) {
                    wordsAl.append("ज")
                    wordsAl.append("ज़")
                } else {
                    size = wordsAl.count
                    for i in 0..<size {
                        str = wordsAl[i]
                        let character = Array(str)
                        if (str.count <= 0 || isVowel(c: character[str.count - 1]) || isMatra(c: character[str.count - 1]) || character[str.count - 1] == nil) {
                            wordsAl[i] = str + "ज"
                            wordsAl.append(str + "ज़")
                        } else {
                            if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                wordsAl.append(str + "्ज")
                                wordsAl.append(str + "्ज़")
                            }
                            wordsAl[i] = str + "ज"
                            wordsAl.append(str + "ज़")
                        }
                    }
                }
            } else if (wordCh == "1" || wordCh == "2" || wordCh == "3" || wordCh == "4" || wordCh == "5" || wordCh == "6" || wordCh == "7" || wordCh == "8" || wordCh == "9" || wordCh == "0" || wordCh == "~" || wordCh == "`" || wordCh == "!" || wordCh == "@" || wordCh == "#" || wordCh == "$" || wordCh == "%" || wordCh == "^" || wordCh == "&" || wordCh == "*" || wordCh == "(" || wordCh == ")" || wordCh == "_" || wordCh == "-" || wordCh == "+" || wordCh == "=" || wordCh == "{" || wordCh == "}" || wordCh == "[" || wordCh == "]" || wordCh == nil || wordCh == ":" || wordCh == "\"" || wordCh == "\"" || wordCh == "<" || wordCh == "," || wordCh == ">" || wordCh == "." || wordCh == "?" || wordCh == "/") {
                size = wordsAl.count
                for i in 0..<size {
                    wordsAl[i] = wordsAl[i] + String(wordCh)
                }
            }
            index = index + 1
        }
        return removeDuplicate(arrayList: wordsAl)
    }
    
    private static func removeDuplicate(arrayList: [String]) -> [String] {
        var arrayList:[String] = arrayList
        for i in 0..<arrayList.count {
            for j in i+1..<arrayList.count {
                if (arrayList[i].elementsEqual(arrayList[j])) {
                    arrayList[j] = ""
                }
            }
        }
        var al = [String]()
        for i in 0..<arrayList.count {
            var str:String = arrayList[i]
            if (!str.elementsEqual("")) {
                al.append(str)
            }
        }
        return al
    }
}
