//
//  NepaliStaticMappings.swift
//  NepaliDictionary
//
//  Created by Suraj Bhadari on 5/15/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import Foundation

class NepaliStaticMappings {
    class func getiW() -> Dictionary<String, String> {
    var iW1 = Dictionary<String, String>()
    iW1["cha"] = "chha"
    iW1["chu"] = "chhu"
    iW1["chha"] = "chha"
    iW1["ma"] = "ma"
    iW1["aba"] = "aba"
    iW1["nam"] = "naam"
    iW1["ram"] = "raam"
    iW1["pani"] = "pani"
    iW1["lai"] = "laaii"
    iW1["pai"] = "paaii"
    iW1["dai"] = "daaii"
    iW1["bhai"] = "bhaaii"

    return iW1;
    }
    
    class func getCr() -> Dictionary<String, String> {
    
        var cR1 = Dictionary<String, String>()
    
    cR1["ba"] = "2348"
    cR1["bha"] = "2349"
    cR1["ca"] = "2325"
    cR1["cha"] = "2330"
    cR1["chha"] = "2331"
    cR1["Da"] = "2337"
    cR1["da"] = "2342"
    cR1["dha"] = "2343"
    cR1["Dha"] = "2338"
    cR1["fa"] = "2347"
    cR1["ga"] = "2327"
    cR1["gha"] = "2328"
    cR1["gya"] = "2332+2381+2334"
    cR1["ha"] = "2361"
    cR1["ja"] = "2332"
    cR1["jha"] = "2333"
        cR1["ka"] = "2325"
    cR1["kha"] = "2326"
    cR1["ksha"] = "2325+2381+2359"
    cR1["la"] = "2354"
    cR1["ma"] = "2350"
    cR1["Na"] = "2339"
    cR1["na"] = "2344"
    cR1["Nepala"] = "2344+2375+2346+2366+2354"
    cR1["nga"] = "2329"
    cR1["pa"] = "2346"
    cR1["pha"] = "2347"
    cR1["qa"] = "2325"
    cR1["ra"] = "2352"
    cR1["sa"] = "2360"
    cR1["sha"] = "2358"
    cR1["Sha"] = "2359"
    cR1["ta"] = "2340"
    cR1["Ta"] = "2335"
    cR1["Tha"] = "2336"
    cR1["tha"] = "2341"
    cR1["va"] = "2357"
    cR1["wa"] = "2357"
    cR1["xa"] = "2325+2381+2360"
    cR1["ya"] = "2351"
    cR1["yna"] = "2334"
    cR1["za"] = "2332"
    
    return  cR1;
    }
    
    class func getUd() -> Dictionary<String, String> {
    var uD1 = Dictionary<String, String>()
    uD1["*"] = "2306"
    uD1["**"] = "2305"
    uD1["."] = "2404"
    uD1["\\"] = "2381"
    /*        uD1["0"] = "2406"
     uD1["1"] = "2407"
     uD1["2"] = "2408"
     uD1["3"] = "2409"
     uD1["4"] = "2410"
     uD1["5"] = "2411"
     uD1["6"] = "2412"
     uD1["7"] = "2413"
     uD1["8"] = "2414"
     uD1["9"] = "2415"*/
    uD1["a"] = "2309"
    uD1["aa"] = "2310"
    uD1["ai"] = "2320"
    uD1["am"] = "2309+2381"
    uD1["au"] = "2324"
    uD1["aum"] = "2384"
    uD1["e"] = "2319"
    uD1["i"] = "2311"
    uD1["ii"] = "2312"
    uD1["o"] = "2323"
    uD1["om"] = "2384"
    uD1["oo"] = "2314"
    uD1["ri^"] = "2381+2352+2367+"
    uD1["rr"] = "2352+2381+8205"
    uD1["rree"] = "2400"
    uD1["rri"] = "2315"
    uD1["u"] = "2313"
    
    return uD1;
    }
}
