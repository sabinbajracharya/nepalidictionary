//
//  Utils.swift
//  NepaliDictionary
//
//  Created by Sabin Bajracharya on 6/3/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import Foundation

public extension String {
    
    public func convertToUnicode() -> String {
        let list = self.split(separator: "+")
        var result = ""
        for item in list {
            let number = Int(item) ?? 0
            let unicode = Unicode.Scalar(number)
            
            if let _ = unicode {
                let content = String(unicode!)
                result.append(content)
            }
        }
        return result
    }
    
}
