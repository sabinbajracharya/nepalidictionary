//
//  DictionaryValue.swift
//  NepaliDictionary
//
//  Created by Sabin Bajracharya on 5/13/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//
struct DictionaryValue {
    public let usageCount: Int
    public let value: String
    
    init (usageCount: Int, value: String) {
        self.usageCount = usageCount;
        self.value = value;
    }
}
