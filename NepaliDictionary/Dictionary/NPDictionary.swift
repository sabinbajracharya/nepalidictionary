//
//  NPDictionary.swift
//  NepaliKeyboard
//
//  Created by Sabin Bajracharya on 5/15/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import Foundation

let smartConvert = true

var uD = NepaliStaticMappings.getUd()
var cR = NepaliStaticMappings.getCr()
var iW = NepaliStaticMappings.getiW()

public class NPDictionary {
    
    public init(){
        
    }


public func translateToNepali(text: String, html: Bool) -> String {
    for (conso, value) in cR {
        if uD[conso] == nil { uD[conso] = value }
        if uD[conso + "a"] == nil { uD[conso + "a"] = value + "+2366" }
        
        let consoMinusA = String(conso[0..<(conso.count - 1)])
        
        if uD[consoMinusA + "i"] == nil { uD[consoMinusA + "i"] = value + "+2367" }
        if uD[consoMinusA + "ee"] == nil { uD[consoMinusA + "ee"] = value + "+2368" }
        if uD[consoMinusA + "u"] == nil { uD[consoMinusA + "u"] = value + "+2369" }
        if uD[consoMinusA + "oo"] == nil { uD[consoMinusA + "oo"] = value + "+2370" }
        if uD[consoMinusA + "ri"] == nil { uD[consoMinusA + "ri"] = value + "+2371" }
        if uD[consoMinusA + "e"] == nil { uD[consoMinusA + "e"] = value + "+2375" }
        if uD[consoMinusA + "ai"] == nil { uD[consoMinusA + "ai"] = value + "+2376" }
        if uD[consoMinusA + "o"] == nil { uD[consoMinusA + "o"] = value + "+2379" }
        if uD[consoMinusA + "au"] == nil { uD[consoMinusA + "au"] = value + "+2380" }
        if uD[consoMinusA] == nil { uD[consoMinusA] = value + "+2381" }
    }
    
    let wordsList = text.components(separatedBy: " ")
    var rVal = ""
    var subs: [String] = []
    
    
    for wordAtIndex in wordsList {
        var word = wordAtIndex.replaceAll(pattern: "ri^", replaceWith: "ari^")
        if smartConvert {
            if hasSW(s: word) {
                
            } else if iW[word] != nil {
                word = iW[word]!
            }else if word.count > 3 {
                var ec_0, ec_1, ec_2, ec_3: Character
                ec_0 = word[word.count - 1].lowercase()!
                ec_1 = word[word.count - 2].lowercase()!
                ec_2 = word[word.count - 3].lowercase()!
                ec_3 = word[word.count - 4].lowercase()!
                
                if ((ec_0 == "a" || ec_0 == "e" || ec_0 == "u") && ec_1 == "h" && ec_2 == "c") {
                    word = word[0..<(word.count-3)] + "chh" + String(ec_0)
                } else if (ec_0 == "y") {
                    word = word[0..<(word.count-1)] + "ree"
                } else if (ec_0 == "a" && ec_1 == "h" && ec_2 == "h") {
                    
                } else if (ec_0 == "a" && ec_1 == "n" && ec_2 == "k") {
                    
                } else if (ec_0 == "a" && ec_1 == "n" && ec_2 == "h") {
                    
                } else if (ec_0 == "a" && ec_1 == "n" && ec_2 == "r") {
                    
                } else if (ec_0 == "a" && ec_1 == "r" && ec_2 == "d" && ec_3 == "n") {
                    
                } else if (ec_0 == "a" && ec_1 == "r" && ec_2 == "t" && ec_3 == "n") {
                    
                } else if (ec_0 == "a" && ((ec_1 == "m") || (!isVowel(ec_1) && !isVowel(ec_3) && ec_1 != "y" && ec_2 != "e"))) {
                    word += "a"
                }
                
                if (ec_0 == "i" && !isVowel(ec_1)) {
                    word = word[0..<(word.count - 1)] + "ee"
                }
            }
        }
        subs = word.components(separatedBy: "/")
        for sub in subs {
            if (sub.count != 0) {
                rVal += getAllUnicode(s: sub, html: html);
            }
        }
    }
    return rVal
}

func hasSW(s: String) -> Bool {
    var sIndex: Int?
    for index in stride(from: s.count - 2, through: 0, by: -1) {
        //        if (getStringValue(s.substring(sIndex)) != null) {
        //            return true; //if (mappingList[s.substring(sIndex)]) return true;
        //        }
    }
    return false;
}

func isVowel(_ letter: Character) -> Bool {
    let c = letter.lowercase()!
    return c == "a" || c == "e" || c == "i" || c == "o" || c == "u"
}

func getAllUnicode(s: String, html: Bool) -> String {
    var allUnicode = ""
    var u: String?
    var tryString = s
    
    tryString = tryString
        .replaceAll(pattern: "T", replaceWith: "~~t~~")
        .replaceAll(pattern: "D", replaceWith: "~~d~~")
        .replaceAll(pattern: "N", replaceWith: "~~n~~")
        .replaceAll(pattern: "SH", replaceWith: "~~sh~~")
        .replaceAll(pattern: "Sh", replaceWith: "~~sh~~")
    
    tryString = tryString.lowercased()
    
    tryString = tryString
        .replaceAll(pattern: "~~t~~", replaceWith: "T")
        .replaceAll(pattern: "~~d~~", replaceWith: "D")
        .replaceAll(pattern: "~~n~~", replaceWith: "N")
        .replaceAll(pattern: "~~sh~~", replaceWith: "Sh")
    
    var nextTryString = ""
    
    while (tryString.count > 0) {
        u = uD[tryString]
        
        if u != nil || tryString.count <= 1 {
            if u != nil {
                allUnicode += getUnicode(t: u!, ll: !(nextTryString.replaceAll(pattern: "^\\s+|\\s+|\\$", replaceWith: "").count > 0), html: html);
            } else {
                allUnicode += tryString;
            }
            tryString = nextTryString;
            nextTryString = ""
        } else {
            nextTryString = String(tryString[tryString.count - 1]) + nextTryString
            tryString = tryString[0..<(tryString.count-1)] + ""
        }
    }
    if (allUnicode.count == 0) {
        return s
    } else {
        return allUnicode
    }
}

func getUnicode(t: String, ll: Bool, html: Bool) -> String {
    var u = "";
    var stopPos = 0;
    var ar: [String] = t.components(separatedBy: "+")
    
    if ll && ar != nil && ar.count >= 1 && smartConvert == true && ar[ar.count - 1] == "2381" {
        stopPos = 1
    }
    if (ar.count > 0) {
        for (index, data) in ar.enumerated() {
            if index >= (ar.count - stopPos) { break }
            if data.count > 0 && !html {
                u += fromCharCode(codePoints:Int(data)!)
            } else if data.count > 0 && html {
                u += "&#" + data + ";"
            }
        }
        
    }
    return u;
}

func fromCharCode(codePoints: Int) -> String {
    return String(UnicodeScalar(codePoints)!)
}

func getSuggestions(string: String) -> [String] {
    
    var returnValue = [String]()
    
//    var dictValues:[DictionaryValue] = dictionary.find(string);
//    Collections.sort(dictValues, new Comparator<DictionaryValue>() {
//        func compare(o1: DictionaryValue, o2: DictionaryValue) -> Int {
//            return o1.getUsageCount() - o2.getUsageCount();
//        }
//    });
//    
//    //        for (DictionaryValue val: userValues) {
//    //            returnValue.add(getUnicode(val.getValue(), true, false));
//    //        }
//
//    for (DictionaryValue val : dictValues) {
//        returnValue.add(getUnicode(val.getValue(), true, false));
//    }
    
    return returnValue;
}
}

extension Character {
    
    var asciiValue: Int {
        get {
            let s = String(self).unicodeScalars
            return Int(s[s.startIndex].value)
        }
    }
    
    func lowercase() -> Character? {
        //        guard let key = (character as String)._core.asciiBuffer?[0] else { return nil }
        let key = self.asciiValue
        return Character(Unicode.Scalar(key < 97 ? key + 32 : key)!) // use < + to lowercase
    }
}

extension String {
    func replaceAll (pattern: String, replaceWith: String = "") -> String{
        do {
            let regex = try NSRegularExpression(pattern: pattern) //, options: NSRegularExpression.Options.)
            let range = NSMakeRange(0, self.count)
            return regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        } catch {
            return ""
        }
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    subscript (bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ..< end]
    }
    subscript (bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ... end]
    }
    subscript (bounds: CountablePartialRangeFrom<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(endIndex, offsetBy: -1)
        return self[start ... end]
    }
    subscript (bounds: PartialRangeThrough<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ... end]
    }
    subscript (bounds: PartialRangeUpTo<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ..< end]
    }
}
extension Substring {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    subscript (bounds: CountableRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ..< end]
    }
    subscript (bounds: CountableClosedRange<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[start ... end]
    }
    subscript (bounds: CountablePartialRangeFrom<Int>) -> Substring {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(endIndex, offsetBy: -1)
        return self[start ... end]
    }
    subscript (bounds: PartialRangeThrough<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ... end]
    }
    subscript (bounds: PartialRangeUpTo<Int>) -> Substring {
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return self[startIndex ..< end]
    }
}

