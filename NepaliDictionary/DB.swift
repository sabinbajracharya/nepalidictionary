//
//  DB.swift
//  NepaliDictionary
//
//  Created by Sabin Bajracharya on 5/8/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//
import SwiftStore

public class DB : SwiftStore {
    /* Shared Instance */
//    public static let store = DB()
    
    static var stores: [String: DB] = [:]
    
    init(withName: String) {
        print("hello")
        super.init(storeName: withName)
    }
    
    public static func getDB(withName: String) -> DB {
        if stores[withName] != nil {
            return stores[withName]!
        } else {
            let db = DB(withName: withName)
            stores[withName] = db
            return db
        }
    }
}
