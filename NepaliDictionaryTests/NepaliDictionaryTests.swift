//
//  NepaliDictionaryTests.swift
//  NepaliDictionaryTests
//
//  Created by Sabin Bajracharya on 5/6/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import XCTest
@testable import NepaliDictionary

class NepaliDictionaryTests: XCTestCase {
    
    var db: DB!
    var content: String!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        FileResource.copyDBResourceToDocumentPath(dbName: nil)
         db = DB.getDB(withName: DBName.phDB)
        content = FileResource.readDataFromFile(file: "nepengmap")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
        
    func testVowel(c: Character) -> Bool {
        let vowel:[Character] = ["ऑ", "अ", "आ", "इ", "ई", "उ", "ऊ", "ऋ", "ओ", "औ", "ए", "ऐ", "ऄ", "ऑ", "ऒ", "ऍ", "ऎ", "०", "१", "२", "३", "४", "५", "६", "७", "८", "९"]
        var b:Bool = false
        for c2:Character in vowel {
            if (c == c2) {
                b = true
            }
        }
        return b
    }
    
    func testMatra(c: Character) -> Bool {
        let matras:[Character] = ["ॉ", "'ं", "'ँ", "ा", "ि", "ी", "ु", "ू", "'ृ", "ो", "ौ", "'े", "'ै", "।", "·", "ॉ", "ॊ", " ँ", "'ॆ", " ँ", " ं"]
        var b:Bool = false
        for c2:Character in matras {
            if (c == c2) {
                b = true
            }
        }
        return b
    }
    
    func testConsonent(c: Character) -> Bool {
        let consonents:[Character] = ["क", "ख", "ग", "घ", "च", "छ", "ज", "झ", "ञ", "ट", "ठ", "ड", "ढ", "ण", "त", "थ", "द", "ध", "न", "प", "फ", "ब", "भ", "म", "य", "र", "ल", "व", "श", "ष", "स", "ह", "क़", "ख़", "ग़", "ज़", "ड़", "ढ़", "फ़", "य़"]
        var b:Bool = false
        for ch:Character in consonents {
            if (c == ch) {
                b = true
            }
        }
        return b
    }
    func testChar(){
        var words:[String] = ["ram", "shyam", "hari", "suraj", "bhandari", "prajwol", "sayseal", "chawal", "alensh", "maharjan", "sameer", "rozil", "sabin", "sabina"]
        var charOfH = [[String]]()
        var r0 = [[String]]()
        r0.append(["क", "ख"])
        r0.append(["ग", "घ"])
        r0.append(["च", "छ"])
        r0.append(["ज", "झ"])
        r0.append(["त", "थ"])
        r0.append(["द", "ध"])
        r0.append(["ट", "ठ"])
        r0.append(["ड", "ढ"])
        r0.append(["प", "फ"])
        r0.append(["ब", "भ"])
        charOfH = r0
        for i in 0..<words.count{
            var wordsAl = [String]()
            var index:Int = 0
            let characters = Array(words[i])
            while (index < characters.count) {
                let wordCh:Character = characters[index]
                var size = Int()
                var str = String()
                if (wordCh == "a" || wordCh == "A") {
                    if (index == 0) {
                        wordsAl.append("अ")
                    } else if (index > 0) {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count == 1 && character[0] == "अ" && ((characters[index - 1] == "a" || characters[index - 1] == "A") && (characters[index] == "a" || characters[index] == "A"))) {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "आ"
                            } else if (testConsonent(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ा"
                                wordsAl.append(str)
                            } else if (!testVowel(c: character[str.count - 1]) && testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ा"
                                //wordsAl.append(str)
                            }
                        }
                    }
                } else if (wordCh == "e" || wordCh == "E") {
                    if (index == 0) {
                        wordsAl.append("ए")
                    } else if (index > 0) {
                        for i in 0..<wordsAl.count {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (testConsonent(c: character[str.count - 1])) {
                                if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                    wordsAl[i] = str + "ए"
                                } else {
                                    wordsAl[i] = str + "े"
                                }
                            } else if (testMatra(c: character[str.count - 1])) {
                                if ((characters[index - 1] != "e" && characters[index - 1] != "E") || (characters[index] != "e" && characters[index] != "E")) {
                                    wordsAl[i] = str + "ए"
                                } else if (character[str.count - 1] == "े") {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ी"
                                }
                            } else if (testVowel(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ए"
                            }
                        }
                    }
                } else if (wordCh == "i" || wordCh == "I") {
                    if (index == 0) {
                        wordsAl.append("इ")
                    } else if (index > 0) {
                        size = wordsAl.count
                        var al = [String]()
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (character[str.count - 1] == "र") {
                                wordsAl[i] = str + "ि"
                                if (str.count == 1) {
                                    al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऋ")
                                }
                                if (str.count > 1 && (character[str.count - 2] == "स" || character[str.count - 2] == "ह" || character[str.count - 2] == "क" || character[str.count - 2] == "ग" || character[str.count - 2] == "घ" || character[str.count - 2] == "त" || character[str.count - 2] == "द" || character[str.count - 2] == "न" || character[str.count - 2] == "प" || character[str.count - 2] == "म" || character[str.count - 2] == "व" || character[str.count - 2] == "ष")) {
                                    al.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ृ")
                                }
                            } else if (index == 1 && ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "अ")) {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ऐ"
                            } else if ((characters[index - 1] == "a" || characters[index - 1] == "A") && character[str.count - 1] == "ा") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ै"
                            } else if (testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ई"
                            } else if (testVowel(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ई"
                            } else if (testConsonent(c: character[str.count - 1])) {
                                if (characters[index - 1] == "a" && characters[index - 1] == "A") {
                                    wordsAl[i] = str + "ई"
                                } else {
                                    wordsAl[i] = str + "ि"
                                }
                            }
                        }
                        for i in 0..<al.count {
                            wordsAl.append(al[i])
                        }
                    }
                } else if (wordCh == "o" || wordCh == "O") {
                    if (index == 0) {
                        wordsAl.append("ओ")
                    } else if (index > 0) {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if ((characters[index] == "o" || characters[index] == "O") && ((characters[index - 1] == "o" || characters[index - 1] == "O") && character[str.count - 1] == "ो")) {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ू"
                            } else if (testConsonent(c: character[str.count - 1]) || character[str.count - 1] == "'़") {
                                if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                    wordsAl[i] = str + "ओ"
                                } else {
                                    wordsAl[i] = str + "ो"
                                }
                            } else if (testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ओ"
                            } else if (testVowel(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ओ"
                            }
                        }
                    }
                } else if (wordCh == "u" || wordCh == "U") {
                    if (index == 0) {
                        wordsAl.append("ऊ")
                    } else if (index > 0) {
                        for i in 0..<wordsAl.count {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (character[str.count - 1] == "ा") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                            } else if (index == 1 && ((characters[index - 1] == "a" || characters[index - 1] == "A") && characters[str.count - 1] == "अ")) {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "औ"
                            } else if (testConsonent(c: character[str.count - 1])) {
                                if (characters[index - 1] == "a" || characters[index - 1] == "A") {
                                    wordsAl[i] = str + "उ"
                                } else {
                                    wordsAl[i] = str + "ू"
                                }
                            } else if (testMatra(c: character[str.count - 1])) {
                                if (character[str.count - 1] == "ौ") {
                                    wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ौ"
                                } else {
                                    wordsAl[i] = str + "ऊ"
                                }
                            } else if (testVowel(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ऊ"
                            }
                        }
                    }
                } else if (wordCh == "b" || wordCh == "B") {
                    if (index == 0) {
                        wordsAl.append("ब")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "'्") {
                                wordsAl[i] = str + "ब"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ब")
                                }
                                wordsAl[i] = str + "ब"
                            }
                        }
                    }
                } else if (wordCh == "c" || wordCh == "C") {
                    if (index == 0) {
                        wordsAl.append("क")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "क"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्क")
                                }
                                wordsAl[i] = str + "क"
                            }
                        }
                    }
                } else if (wordCh == "d" || wordCh == "D") {
                    if (index == 0) {
                        wordsAl.append("द")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "द"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्द")
                                }
                                wordsAl[i] = str + "द"
                            }
                        }
                    }
                } else if (wordCh == "f" || wordCh == "F") {
                    if (index == 0) {
                        wordsAl.append("फ")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "फ"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्फ")
                                }
                                wordsAl[i] = str + "फ"
                            }
                        }
                    }
                } else if (wordCh == "g" || wordCh == "G") {
                    if (index == 0) {
                        wordsAl.append("ग")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "ग"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ग")
                                }
                                wordsAl[i] = str + "ग"
                            }
                        }
                    }
                } else if (wordCh == "h" || wordCh == "H") {
                    if (index == 0) {
                        wordsAl.append("ह")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (characters[index - 1] == "f" || characters[index - 1] == "F" || characters[index - 1] == "r" || characters[index - 1] == "R" || characters[index - 1] == "m" || characters[index - 1] == "M" || characters[index - 1] == "l" || characters[index - 1] == "L" || characters[index - 1] == "n" || characters[index - 1] == "N" || characters[index - 1] == "a" || characters[index - 1] == "A" || characters[index - 1] == "e" || characters[index - 1] == "E" || characters[index - 1] == "i" || characters[index - 1] == "I" || characters[index - 1] == "o" || characters[index - 1] == "O" || characters[index - 1] == "u" || characters[index - 1] == "U" || characters[index - 1] == "v" || characters[index - 1] == "V") {
                                if (str.count <= 0 || character[str.count - 1] == "'़" || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1])) {
                                    wordsAl[i] = str + "ह"
                                } else {
                                    wordsAl[i] = str + "ह"
                                    if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                        wordsAl.append(str + "्ह")
                                    }
                                }
                            } else if (index > 0 && characters[index - 1] != "c" && characters[index - 1] != "C" && characters[index - 1] != "a" && characters[index - 1] != "A" && str.count > 0 && (character[str.count - 1] == "क" || character[str.count - 1] == "ग" || character[str.count - 1] == "च" || character[str.count - 1] == "ज" || character[str.count - 1] == "ट" || character[str.count - 1] == "ड" || character[str.count - 1] == "त" || character[str.count - 1] == "द" || character[str.count - 1] == "प" || character[str.count - 1] == "ब")) {
                                if (characters[index - 1] == "p" || characters[index - 1] == "P") {
                                    wordsAl.append(str + "ह")
                                }
                                for j in 0..<charOfH.count {
                                    if (character[str.count - 1] == Array(charOfH[j][0])[0]) {
                                        wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + String(Array(charOfH[j][1])[0])
                                        //System.out.println("ksh word" + str.substring(0, str.count - 1) + charOfH[j][1].charAt(0))
                                        if (!(str.count <= 1 || testVowel(c: character[str.count - 2]) || testMatra(c: character[str.count - 2]) || (characters[index - 1] == "a" && characters[index - 1] == "A"))) {
                                            wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + charOfH[j][1])
                                        }
                                    }
                                }
                            } else if (characters[index - 1] == "c" || characters[index - 1] == "C") {
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "च"
                            } else if (character[str.count - 1] == "स") {
                                //System.out.println("kunwaravanish out this look of charof h")
                                if (!(str.count <= 1 || testVowel(c: character[str.count - 2]) || testMatra(c: character[str.count - 2]))) {
                                    wordsAl[i] = str + "्" + String(character[str.count - 1])
                                    if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                        wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श")
                                    }
                                }
                                wordsAl[i] = String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "श"
                            }
                        }
                    }
                } else if (wordCh == "j" || wordCh == "J") {
                    if (index == 0) {
                        wordsAl.append("ज")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || character[str.count - 1] == "़" || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ज"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ज")
                                }
                                wordsAl[i] = str + "ज"
                            }
                        }
                    }
                } else if (wordCh == "k" || wordCh == "K") {
                    if (index == 0) {
                        wordsAl.append("क")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "क"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्क")
                                }
                                wordsAl[i] = str + "क"
                            }
                        }
                    }
                } else if (wordCh == "l" || wordCh == "L") {
                    if (index == 0) {
                        wordsAl.append("ल")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "ल"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ल")
                                }
                                wordsAl[i] = str + "ल"
                            }
                        }
                    }
                } else if (wordCh == "m" || wordCh == "M") {
                    if (index == 0) {
                        wordsAl.append("म")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1])) {
                                wordsAl[i] = str + "म"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्म")
                                }
                                wordsAl[i] = str + "म"
                            }
                        }
                    }
                } else if (wordCh == "n" || wordCh == "N") {
                    if (index == 0) {
                        wordsAl.append("न")
                    } else {
                        size = wordsAl.count
                        var arrayList = [String]()
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "न"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्न")
                                }
                                wordsAl[i] = str + "न"
                            }
                        }
                    }
                } else if (wordCh == "p" || wordCh == "P") {
                    if (index == 0) {
                        wordsAl.append("प")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "प"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्प")
                                }
                                wordsAl[i] = str + "प"
                            }
                        }
                    }
                } else if (wordCh == "q" || wordCh == "Q") {
                    if (index == 0) {
                        wordsAl.append("क")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "क"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्क")
                                }
                                wordsAl[i] = str + "क"
                            }
                        }
                    }
                } else if (wordCh == "r" || wordCh == "R") {
                    if (index == 0) {
                        wordsAl.append("र")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "र"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्र")
                                }
                                wordsAl[i] = str + "र"
                            }
                        }
                    }
                } else if (wordCh == "s" || wordCh == "S") {
                    if (index == 0) {
                        wordsAl.append("स")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "स"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्स")
                                }
                                wordsAl[i] = str + "स"
                            }
                        }
                    }
                } else if (wordCh == "t" || wordCh == "T") {
                    if (index == 0) {
                        wordsAl.append("त")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "त"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्त")
                                }
                                wordsAl[i] = str + "त"
                            }
                        }
                    }
                } else if (wordCh == "v" || wordCh == "V") {
                    if (index == 0) {
                        wordsAl.append("व")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "व"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्व")
                                }
                                wordsAl[i] = str + "व"
                            }
                        }
                    }
                } else if (wordCh == "w" || wordCh == "W") {
                    if (index == 0) {
                        wordsAl.append("व")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "व"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्व")
                                }
                                wordsAl[i] = str + "व"
                            }
                        }
                    }
                } else if (wordCh == "x" || wordCh == "X") {
                    if (index == 0) {
                        wordsAl.append("ज")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "ज"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ज")
                                }
                                wordsAl[i] = str + "ज"
                            }
                        }
                    }
                } else if (wordCh == "y" || wordCh == "Y") {
                    if (index == 0) {
                        wordsAl.append("य")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "य"
                                if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                    wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                                }
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्य")
                                }
                                if (character[str.count - 1] == "ग" || character[str.count - 1] == "ज") {
                                    wordsAl.append(String(str[str.index(str.startIndex, offsetBy: str.count - 1)]) + "ज्ञ")
                                }
                                wordsAl[i] = str + "य"
                            }
                        }
                    }
                } else if (wordCh == "z" || wordCh == "Z") {
                    if (index == 0) {
                        wordsAl.append("ज")
                    } else {
                        size = wordsAl.count
                        for i in 0..<size {
                            str = wordsAl[i]
                            let character = Array(str)
                            if (str.count <= 0 || testVowel(c: character[str.count - 1]) || testMatra(c: character[str.count - 1]) || character[str.count - 1] == "्") {
                                wordsAl[i] = str + "ज"
                            } else {
                                if (!(characters[index - 1] == "a" || characters[index - 1] == "A")) {
                                    wordsAl.append(str + "्ज")
                                }
                                wordsAl[i] = str + "ज"
                            }
                        }
                    }
                } else if (wordCh == "1" || wordCh == "2" || wordCh == "3" || wordCh == "4" || wordCh == "5" || wordCh == "6" || wordCh == "7" || wordCh == "8" || wordCh == "9" || wordCh == "0" || wordCh == "~" || wordCh == "`" || wordCh == "!" || wordCh == "@" || wordCh == "#" || wordCh == "$" || wordCh == "%" || wordCh == "^" || wordCh == "&" || wordCh == "*" || wordCh == "(" || wordCh == ")" || wordCh == "_" || wordCh == "-" || wordCh == "+" || wordCh == "=" || wordCh == "{" || wordCh == "}" || wordCh == "[" || wordCh == "]" || wordCh == nil || wordCh == ":" || wordCh == "\"" || wordCh == "\"" || wordCh == "<" || wordCh == "," || wordCh == ">" || wordCh == "." || wordCh == "?" || wordCh == "/") {
                    size = wordsAl.count
                    for i in 0..<size {
                        wordsAl[i] = wordsAl[i] + String(wordCh)
                    }
                }
                index = index + 1
            }
            print("TestWord -\(testremoveDuplicate(arrayList: wordsAl))")
        }
        //        print("TestWord -\(testremoveDuplicate(arrayList: wordsAl))")
    }
    
    func testremoveDuplicate(arrayList: [String]) -> [String] {
        var arrayList:[String] = arrayList
        for i in 0..<arrayList.count {
            for j in i+1..<arrayList.count {
                if (arrayList[i].elementsEqual(arrayList[j])) {
                    arrayList[j] = ""
                }
            }
        }
        var al = [String]()
        for i in 0..<arrayList.count {
            var str:String = arrayList[i]
            if (!str.elementsEqual("")) {
                al.append(str)
            }
        }
        return al
    }
    
    func testAddName() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        db["name"] = "sabin"
        let name = db["name"]
        
        print("Name from DB: \(String(describing: name))")
        assert(name == "sabin", "Name is not equal to sabin")
    }
    
    func testNameSearch() {
//        db["jagat"] = "tall"
//        db["sabin"] = "tall"
//        db["sanish"] = "medium"
//        db["saurav"] = "small"
//        db["sajan"] = "short"
//        db["rosy"] = "short"
//
        
//        print("namearray")
        let keys = db.findKeys(key: "kathmandu")
        dump(keys)
//        let collect = db.collect(key: "sabin")
//        dump(collect)
        assert(1 == 1)
    }
    
    func testCSV() {
        let rows = content.components(separatedBy: "\n")
        for (index, row) in rows.enumerated() {
            if index == 0 { continue }
            let columns = row.components(separatedBy: ",")
            if columns.count == 3 {
                db[columns[2].trimmingCharacters(in: .newlines)] = columns[1]
            }
        }
        dump(db["farak"]!)
        dump(db.findKeys(key: "farak"))
        
        assert(db["farak"] == "फरक", "Acutal word:: \(db["farak"])")
        let result = db.findKeys(key: "farak")
        assert(result.count == 2, "Actual keys count = \(result.count)")
        
        print("List of words that start with 'farak'")
        print(result)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
